% :- table conn/2
% :- table connected/3
% :- table pathf(_,_,lattice(shortest/3))
% :- table pathb(_,_,lattice(shortest/3))

shortest(P1, P2, P) :-
   length(P1, L1),
   length(P2, L2),
   (L1 < L2 -> P = P1; P = P2).

% Right
adjacent(pos(X1,Y1), pos(X2, Y1)) :- succ(X1, X2), X1 < 6.
% Down
adjacent(pos(X1,Y1), pos(X1, Y2)) :- succ(Y1, Y2), Y1 < 4.
% Left
adjacent(pos(X1,Y1), pos(X2, Y1)) :- succ(X2, X1).
% Up
adjacent(pos(X1,Y1), pos(X1, Y2)) :- succ(Y2, Y1).

% Can we go from U to V?
stepf(U, V) :- adjacent(U, V), free(V).
% Can we go from U to U?
stepb(U, V) :- adjacent(U, V), free(U).

connf(U, U).
connf(U, V) :-
  connf(W, V),
  stepf(U, W).

connected(P, P, _).
connected(P, Q, V) :-
  \+ member(P, V),
  stepf(P, R),
  connected(R, Q, [P|V]).

connr :- goal(Q), robot(P), connf(P, Q).
exroute :- goal(Q), robot(P), connected(P, Q, []).

pathf(U, U, [U]).
pathf(U, V, [U|P]) :-
  pathf(W, V, P),
  stepf(U, W).

pathb(U, V, [U,V]) :- stepb(U,V).
pathb(U, V, P) :-
  pathb(U, W, P0),
  stepb(W, V),
  append(P0, [V], P).

routef(P) :- robot(U), goal(V), pathf(U, V, P).
routeb(P) :- robot(U), goal(V), pathb(U, V, P).

robot(pos(2, 3)).
goal(pos(5,1)).

free(pos(1,1)).
free(pos(1,4)).

free(pos(2,2)).
free(pos(2,3)).
free(pos(2,4)).

free(pos(3,1)).
free(pos(3,2)).
free(pos(3,4)).

free(pos(4,1)).
free(pos(4,2)).
free(pos(4,3)).
free(pos(4,4)).

free(pos(5,1)).
free(pos(5,3)).

free(pos(6,1)).
free(pos(6,2)).
free(pos(6,3)).