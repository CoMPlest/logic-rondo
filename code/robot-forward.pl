% :- table path(_,_,lattice(shortest/3))
% :- table conn/2

% Partial order of lists by length; used in tabled execution
shortest(P1, P2, P) :-
   length(P1, L1),
   length(P2, L2),
   (L1 < L2 -> P = P1; P = P2).

% Right
adjacent(pos(X1,Y1), pos(X2, Y1)) :- succ(X1, X2), X1 < 6.
% Down
adjacent(pos(X1,Y1), pos(X1, Y2)) :- succ(Y1, Y2), Y1 < 4.
% Left
adjacent(pos(X1,Y1), pos(X2, Y1)) :- succ(X2, X1).
% Up
adjacent(pos(X1,Y1), pos(X1, Y2)) :- succ(Y2, Y1).

% Can we go from U to V?
step(U, V) :- adjacent(U, V), free(V).

% conn(U, V) holds if two positions U and V connected.
conn(U, U).
conn(U, V) :-
  conn(W, V),
  step(U, W).

% Can our robot reach the goal?
connr :- robot(U), goal(V), conn(U, V).

% path(U, V, P) holds if P is a path from U to V. A path is here a list of positions.
path(U, U, [U]).
path(U, V, [U|P]) :-
  path(W, V, P),
  step(U, W).

% A path P with route(P) leads our robot from the initial position to the goal.
route(P) :- robot(U), goal(V), path(U, V, P).

% Initial position of robot.
robot(pos(2, 3)).

% Position of goal.
goal(pos(5,1)).

% All the positions that do not contain an obstacle.
free(pos(1,1)).
free(pos(1,4)).

free(pos(2,2)).
free(pos(2,3)).
free(pos(2,4)).

free(pos(3,1)).
free(pos(3,2)).
free(pos(3,4)).

free(pos(4,1)).
free(pos(4,2)).
free(pos(4,3)).
free(pos(4,4)).

free(pos(5,1)).
free(pos(5,3)).

free(pos(6,1)).
free(pos(6,2)).
free(pos(6,3)).