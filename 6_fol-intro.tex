\documentclass[logic-rondo]{subfiles}
\begin{document}

\beginchapter{6}{Introduction to First-Order Predicate Logic}{fol-intro}

\newcommand*{\rVar}[2]{\text{r-#1-#2}}
\newcommand*{\oVar}[2]{\text{o-#1-#2}}
\newcommand*{\gVar}[2]{\text{g-#1-#2}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% https://en.wikipedia.org/wiki/Raghunatha_Siromani
% https://en.wikivoyage.org/wiki/Nabadwip
% https://familypedia.wikia.org/wiki/Nadia_district
% https://en.wikipedia.org/wiki/Nadia_district
% http://www.columbia.edu/itc/mealac/pritchett/00islamlinks/ikram/part2_18.html
% http://pragati.nationalinterest.in/2014/04/the-lost-age-of-reason/

% History of Indian Logic
% Handbook of the history of logic.: (Greek, Indian and Arabic logic) by Gabbay, Dov M ; Woods, John
% Indian logic : a reader by Ganeri, Jonardon.

% https://en.wikipedia.org/wiki/P%C4%81%E1%B9%87ini
% Pāṇini: His Work and Its Traditions - George Cardona
% Descriptive Technique of Panini: An Introduction by Misra, Vidya Niwas

% FOL & AI: https://link.springer.com/chapter/10.1007%2F978-81-322-3972-7_3


\direct{
  Clara and Isaac are in front of a grey stone temple, made from of stacked segments that
  narrow down with every level.
  Each segment is decorated with ornaments and large arcs invite visitors to enter.
  Trees grow around and above the temple.
  They provide shade and coolness, protecting from the hot midday sun.
  Clara and Isaac sit down at the foot of one of the trees.
}

\begin{dialogue}
  \spC We had quite the journey so far! What brings us here?
  \spI During our dinner with al-Khwārizmī, I realised that, even though we have learned
  how to reason with propositions, we do not know how to reason about objects and elements
  of this world.
  How can we state that objects have equal properties or relate in other ways?
  \spC What do you mean exactly?
  \spI I can get up, and walk towards the temple without running into any tree, person or other
  obstacle.
  This means I am able to reason about my position relative to any number of obstacles
  and any size of an environment.
  \spC And you are saying that this is not possible with propositional logic?
  \spI Look, let me give an example.
\end{dialogue}

\section{The Need for a Richer Language}
\label{sec:fol-motivation}

\direct{Isaac draws the board in \cref{fig:robot-field} in the sand between him and Clara.}

\begin{figure}[ht]
  \centering
  \newcommand*{\xMin}{1}%
  \newcommand*{\xMax}{6}%
  \newcommand*{\yMin}{1}%
  \newcommand*{\yMax}{4}%
  \begin{tikzpicture}
    \foreach \i in {\xMin,...,\xMax} {
      \draw [very thin,gray] ($(\i,\yMin) + (0,-1)$) -- (\i,\yMax)  node [above] at ($(\i,\yMax)+(0.5,0)$) {$\i$};
    }
    \foreach \i [evaluate = \i as \k using int((\yMax + 1) - \i)] in {\yMin,...,\yMax} {
      \draw [very thin,gray] (\xMin,\i) -- ($(\xMax,\i) + (1, 0)$) node [left] at ($(\xMin,\i) + (0,-0.5)$) {$\k$};
    }
    \draw [very thin,gray] ($(\xMax,\yMin) + (1,-1)$) -- ($(\xMax,\yMax) + (1,0)$);
    \draw [very thin,gray] ($(\xMin,\yMin) + (0,-1)$) -- ($(\xMax,\yMin) + (1,-1)$);

    % \draw [step=1.0,blue, very thick] (0.5,0.5) grid (5.5,4.5);
    % \draw [very thick, brown, step=1.0cm,xshift=-0.5cm, yshift=-0.5cm] (0.5,0.5) grid +(5.5,4.5);

    \newcommand*{\robotPos}{(2,3)}%
    \draw let \p1=\robotPos in
    node[inner sep=0pt] at ($(\x1,\yMax + 1) - (0, \y1) + (0.5, -0.5)$) {
      \includegraphics[width=0.8cm]{robot.pdf}
    };

    \newcommand*{\heartPos}{(5,1)}%
    \draw let \p1=\heartPos in
    node[inner sep=0pt] at ($(\x1,\yMax + 1) - (0, \y1) + (0.5, -0.5)$) {
      \includegraphics[width=0.4cm]{heart.pdf}
    };

    \foreach \position in {(2,1), (1,2), (1,3), (3,3), (5,2), (5,4), (6,4)} {
      \draw let \p1=\position in
      node[inner sep=0pt] at ($(\x1,\yMax + 1) - (0, \y1) + (0.5, -0.5)$) {
        \includegraphics[width=1cm]{obstacle.pdf}
      };
    }
  \end{tikzpicture}
  \caption{Robot trying to find a heart}
  \label{fig:robot-field}
\end{figure}

\begin{dialogue}
  \spC Do you think you are a star?
  \spI At least you recognise that this is a robot and what it wants!
  What you see on this board is me, some obstacles and a goal that I am trying to reach.
  In other words, we are trying to find a route from the initial position to the goal.
  \spC Ok, let me attempt to solve this using propositional logic.

  First, we introduce propositional variables for all the positions $(x,y)$ in your drawing, where
  $x$ ranges from $1$ to $6$, and $y$ from $1$ to $4$.
  \begin{align*}
    & \rVar{x}{y} & & \text{Robot in position } (x,y) \\
    & \oVar{x}{y} & & \text{Obstacle in position } (x,y) \\
    & \gVar{x}{y} & & \text{Goal in position } (x,y)
  \end{align*}
  We can then specify, for example, that you are in position $(2,3)$ by stipulating that the
  variable $\rVar{2}{3}$ is true.
  Or something more complex: The requirement that you should never collide with an obstacle
  can be expressed by the following formula.
  \begin{equation}
    \label{eq:robot-safety-pl}
    (\rVar{1}{1} \to \neg \oVar{1}{1})
    \land (\rVar{1}{2} \to \neg \oVar{1}{2})
    \land (\rVar{1}{3} \to \neg \oVar{1}{3})
    \land \dotsm
  \end{equation}

  Oh, I see the problem! This specification consists of $24$ conjuncts and only
  works for this specific board size.
  Is there a better way to do this?
  \spI I don't know. My CPU is running hot! This is not the season for me to be in India.
  \spC Shall we go inside? The temple should allow you to cool down.
  \spI Lets go!
\end{dialogue}

\direct{
  Clara and Isaac enter the temple through the main arc in the middle.
  Music and discussions immediately enshroud and drag them in.
}

\begin{dialogue}
  \spC These drums, lutes and pipes sound very familiar.
  \spI The Muslim culture has expanded far to the east!
  \spRag I'm not here for \emph{Jalpa}! This is a serious issue and there is no place for
  wrangling! Let us continue this later \dots{}

  Who are you two strange figures??
  \spC My friend Isaac and I came inside to cool down a bit from thinking about a difficult
  logic problem.
  \spRag Logic? I hope you don't mean the kind of logic employed in the debate over there:
  arguing purely for winning and not for the good of our society, and entirely neglecting facts and
  arguments?
  \spI You seem to be upset \dots{}
  \spRag Please excuse my temper!
  \spC We are in fact here because we do not understand some aspect of Isaac's inner machinery.
  \spRag Is this a \dots{}?
  \spC Yes.
  \spRag Marvellous! Tell me, what is your problem?

  \direct{They explain to Raghunātha the problem that they had just discussed outside
    the temple.
  }
  \spRag I see. Let us sit down in the quiet corner over there and enjoy a friendly discussion
  over some refreshments.
\end{dialogue}

What you need is a language for talking about the properties of objects and not just propositions.
This is the language of \emph{first-order predicate logic} (FOL).
Predicates allow us to express properties of objects.
For example, instead of having a propositional variable for every position in \cref{fig:robot-field}
and every property of that position, we have just \emph{one predicate per property} that classifies
the positions.
Concretely, the variables $\rVar{x}{y}$, $\oVar{x}{y}$ and $\gVar{x}{y}$ can be replaced in FOL
with predicates $R$, $O$ and $G$ on positions.
For the moment, we will hide the $x$- and $y$-coordinates and just write $p$ for a position $(x,y)$.
The predicates $R$, $O$ and $G$ on positions are then used and read as follows.
\begin{align*}
  & R(p) & & \text{Robot in position } p \\
  & O(p) & & \text{Obstacle in position } p \\
  & G(p) & & \text{Goal in position } p
\end{align*}

You could now naively write the formula \eqref{eq:robot-safety-pl} in the same way, but then we
would have not gained anything.
Instead, the power of predicate logic comes from the fact that there are logical connectives that
allow you to talk about \emph{objects}, which are in this case positions on the board.
In particular, \cref{eq:robot-safety-pl} states that Isaac should not collide with an obstacle
on \emph{all positions}:
\begin{equation}
  \label{eq:no-collision-sentence}
  \begin{split}
    & \text{For all positions } p \text{, if the robot is in } p, \\
    & \text{then there should be no obstacle in } p.
  \end{split}
\end{equation}
Clearly, we could use propositional connectives and the predicates $R$ and $O$ to represent
$\text{if the robot is in } p \text{, then there should be no obstacle in } p$
by the formula
$R(p) \to \neg O(p)$.
What remains is to find a formal representation for the phrase ``For all positions $p$''.
First-order predicate logic uses a logical connective, called the \emph{universal quantifier},
that provides exactly this representation.
This quantifier is written as $\forall$, a turned around A, and comes with an
\emph{object variable} behind that it binds.
We will also use a dot to separate the quantifier with the bound variables from the remaining
formula.
The sentence \eqref{eq:no-collision-sentence} becomes then this symbolic expression
\begin{equation}
  \label{eq:no-collision-formula}
  \all{p} R(p) \to \neg O(p) \, ,
\end{equation}
which we read as ``for all $p$, if $R(p)$ then not $O(p)$''.

\begin{dialogue}
  \spC Can you explain a bit more why we use the dot?
  \spRag It indicates the \emph{scope} of the universal quantifier: the quantifier and the object
  variable $p$ refer to everything on the right of the dot.
  This helps to reduce the use of parentheses, as we would have to write
  $\all{p} (R(p) \to \neg O(p))$ otherwise.
  You also see that people write $(\forall p) (R(p) \to \neg O(p))$, but
  the number of parentheses gets out of control with this approach.
  That's why we use the dot with the convention that everything on its right is in its scope.
  \spC What is actually an \emph{object} variable?
  \spRag Excellent questions! We will come to these soon.
  For the time being, think of object variables as placeholders for objects of unknown nature.
  These can be positions, numbers, letters in an alphabet or any other mathematically
  presentable object.
  \spC Alright\dots{} Isaac and I began with the problem of finding a route on the
  board \cref{fig:robot-field}.
  Can we use FOL for this as well?
  \spRag Yes, of course! In fact, this is a problem that shows that FOL can express things that
  are \emph{impossible} to express with propositional logic.
\end{dialogue}

Suppose, we want to specify what a route from the starting position to the goal is in
\cref{fig:robot-field}.
If you attempt this in propositional logic, then you will find that there are infinitely many
possible routes, even for this finite board, because the robot can run in loops.
Therefore, a formula that describes how a route looks like would have to be infinite in
propositional logic, which is clearly non-sense!
Now, think about how you would describe a route: it is path on the board leads from the starting
point to the goal, with the crucial property that
\begin{equation}
  \label{eq:path-sentence}
  \text{There is no obstacle anywhere on a path.}
\end{equation}
Let us attempt to formalise~\eqref{eq:path-sentence} using FOL.
First, we have to choose a way to talk about paths.
There are many possibilities to do that, but let us see a path as a list of positions on the
board that we can access by indices, just like arrays in programming.
Thus, we find at the index 0 the initial position of a path, at index 1 the next position etc.
Formally, we use three more predicates:
\begin{align*}
  & P(r) & & r \text{ is a path} \\
  & N(k) & & k \text{ is an index (natural number)} \\
  & S(r,k,p) & & \text{Position } p \text{ occurs at index } k \text{ in } r
\end{align*}
Using these predicates, we can express that there is no obstacle on a path, as in
\eqref{eq:path-sentence}, by the following formula.
\begin{equation}
  \label{eq:path-formula-naive}
  \all{r} P(r) \to
  \all{k} \all{p} N(k) \land S(r, k, p) \to \neg O(p)
\end{equation}
In words, this formula is read as
\begin{quote}
  ``For all paths $r$ and for all $k$ and $p$, if $k$ is an index and
  $p$ is the position in $r$ at index $k$, then there is no obstacle at position $p$.''
\end{quote}

\begin{dialogue}
  \spRag Easy, right?
  \spC Looks rather complicated! How does this work with the dots again?
\end{dialogue}

The easiest way is to imagine that you add parentheses around everything on the right
of the dot, until you get to some parentheses.
If you do this, starting from the outside, then you get the formula
\begin{equation*}
  \all{r} \parens[\Bigg]{
    P(r) \to
    \parens[\Big]{
      \all{k} \all{p} \parens[\big]{N(k) \land S(r, k, p) \to \neg O(p)}
    }
  } \, .
\end{equation*}

\begin{dialogue}
  \spI So far so good. But now I would like to interpret, at least intuitively, this formula.
  Suppose I assume that $S(r, 0, (2,3))$ is true, which is the initial position on the board
  in \cref{fig:robot-field} and that
  $S(r, 1, (2,2))$ \emph{and} $S(r, 1, (3,3))$ are true.
  Does this mean that I am in some kind of undecided quantum state, occupying both positions
  $(2,2)$ and $(3,3)$ after the first step?
  Even worse, in one case I hit my head against an obstacle!
  \spRag This is a very good observation and is a flaw in our naive formalisation.
\end{dialogue}

Fortunately, first-order predicate logic has a way out.
Notice that $p$ is \emph{uniquely determined} by $r$ and $k$ in $S(r,k,p)$,
and thus $S$ is a map.
In FOL, we allow ourselves to write such maps in the form of so-called
\emph{function symbols}.
For this particular example, we can use one symbol $s$ with the following meaning.
\begin{equation*}
  s(r,k) \text{ returns the position at index } k \text{ in path r}
\end{equation*}
We can then simplify \cref{eq:path-formula-naive} to
\begin{equation}
  \label{eq:path-formula-better}
  \all{r} P(r) \to \all{k} N(k) \to \neg O(s(r,k))
\end{equation}
and thereby avoid that a path can be ambiguous.
Note, however, that such ambiguities might be wanted and that this is the crucial difference
between using predicates and functions:
\begin{itemize}
\item use a predicate (also called relation) $S$, if there can be multiple positions (or none)
  at index $k$ on the path $r$, and
\item use a map $s$, if there is exactly one position for every index $k$ on all paths $r$.
\end{itemize}

\begin{dialogue}
  \spI Oh, isn't there then a problem with the formula \eqref{eq:path-formula-better}?
\end{dialogue}
\begin{quiz}
  Can you see what problem Isaac spotted? And would you have an idea how to solve it?
  Hint: The predicate $N$ only ensures that $k$ is a natural number, but does not relate $k$
  to the length of the path $r$.
\end{quiz}
\begin{answer}
  In \cref{eq:path-formula-better}, we used that $s(r,k)$ is supposedly uniquely defined
  for any $k$.
  But typically, path would be of finite length, say $l$.
  If $k$ is larger than $l$, the map $s$ cannot be defined for $s(r,k)$.

  There are several ways to solve this.
  A more elegant one will be presented in \cref{chap:fol-automation}, but that approach uses a
  different definition of paths and indexing entirely.
  For our present definition, we can introduce a function symbol $\mathrm{len}$, which returns
  the \textbf{len}gth of a path, and a predicate symbol $B$, where $B(k,n)$ intuitively holds if
  $k$ is natural number below the \textbf{B}ound $n$.
  The formula~\eqref{eq:path-formula-better} can then be improved to
  \begin{equation*}
    \all{r} P(r) \to \all{k} B(k, \mathrm{len}(r)) \to \neg O(s(r,k)) \, .
  \end{equation*}
\end{answer}

\begin{dialogue}
  \spI Ok, so that problem is solvable.
  We talked initially about routes, which supposedly lead from the starting point
  to the goal, but now we diverted to paths.
  How can we put this together?
  \spRag Which question is it that we are actually trying answer?
  \spC We began with the routing problem, which asks for a route from the initial
  position of the robot to the heart.
  \spRag Yes indeed. It is important to realise that this problem asks for \emph{the existence}
  of a path.
  So far, we have only dealt with \emph{universal} statements, statements that require something
  to hold \emph{for all} instances of an object variable.
  But existence is qualitatively different.
\end{dialogue}

To be precise, let us formulate the routing problem first in natural language:
\begin{equation}
  \label{eq:routing-sentence}
  \text{There is a path leading from the initial to the goal position.}
\end{equation}
The phrase ``there is'' indicates that we are looking for the existence of something.
First-order predicate logic allows us to express such statements with the so-called
\emph{existential quantifier}, written as ``$\exists$'' (a turned around ``E'').
To formulate the sentence~\eqref{eq:routing-sentence} using this quantifier, let us use
two more function symbols:
\begin{align*}
  & \zero & & \text{represents the number 0} \\
  & e(r)  & & \text{last index in path } r
\end{align*}

With these function symbols, we can present the sentence~\eqref{eq:routing-sentence}
as the following formula.
\begin{equation}
  \label{eq:routing-formula}
  \exist{r} P(r) \land R(s(r, \zero)) \land G(s(r, e(r)))
\end{equation}
This formula can be read like this:
\begin{equation*}
  \begin{aligned}
    & \underbrace{\exist{r} P(r)}_{\text{there is a path}} & \land &
    \underbrace{R(s(r, \zero))}_{\text{starting at the robot position}} & \land &
    & \underbrace{G(s(r, e(r)))}_{\text{ending at the goal}}
  \end{aligned}
\end{equation*}
With this at hand, you should be able to easily match the formula with the informal sentence that
describes the routing problem.

\begin{dialogue}
  \spC Whew! This seems all quite reasonable, but what is first-order predicate logic
  then actually?
  \spRag I'm glad you should ask. Let us try to distil a formal definition of first-order formulas.
\end{dialogue}

\section{The Language of First-Order Logic}
\label{sec:fol-syntax}

In propositional logic, we only deduced logical facts and we were thus content with formulas
that were made up of propositional variables and logical connectives.
In first-order predicate logic, or FOL, we additionally wish to reason about the relations of
objects.
Thus, FOL will have two building blocks:
\begin{itemize}
\item \inlinedefbox{terms}, built from object variables and function symbols, and
\item \inlinedefbox{formulas}, built from predicate symbols and logical connectives.
\end{itemize}
Terms will thereby be the syntactical representation of the objects that we aim to reason
about.
As you can see, both terms and formulas mention \emph{symbols}.
Looking back at the function and predicate symbols that we used in the formalisation of the routing
problem, you will see that every symbol can be applied to a certain number of arguments.
For instance, $s$ was a symbol that can be applied to two arguments, while $O$ only took one
argument.
We refer to the number of arguments of a symbol as its \emph{arity}.
Formally, the symbols that can appear in FOL formulas come from so-called signatures:
\begin{definition}{}{fol-signature}
  A first-order \termdef{signature} $\Lang$ is a triple \inlinedefbox{$(\FSym, \PSym, \ar)$}, where
  $\FSym$ and $\PSym$ are disjoint sets ($\FSym \cap \PSym = \emptyset$) and $\ar$ is a map
  $\FSym \cup \PSym \to \N$.
  The elements of $\FSym$ are called \termdef{function symbols} and those of $\PSym$
  \termdef{predicate symbols} or \notion{relation symbols}.
  The map $\ar$ assigns to each symbols its \termdef{arity}, which is the number of argument the
  symbol expects.
  We write \inlinedefbox{$\FSym_{n} = \setDef{f \in \FSym}{\ar(f) = n}$} and
  \inlinedefbox{$\PSym_{n} = \setDef{P \in \PSym}{\ar(P) = n}$}.
  The elements of $\FSym_{0}$ are called \notion{constants}.
\end{definition}
Note that $\FSym$ (and $\PSym$) is partitioned into the sets $\FSym_{n}$, that is,
$\FSym = \bigcup_{n \in \N}\FSym_{n}$ and $\FSym_{n} \cap \FSym_{m} = \emptyset$ for $m \neq n$.
This is because $\ar$ is a map and thus assigns to every symbol a unique arity.

Let us illustrate this definition on the routing problem.
\begin{example}{}{routing-signature}
  The signature $\Lang = (\FSym, \PSym, \ar)$ of all symbols that we used in
  \cref{sec:fol-motivation} is given as follows.
  \begin{align*}
    & \FSym = \set{s, \zero, e}
    \qquad
    \PSym = \set{R, G, O, P, N, S} \\
    & \ar(\zero) = 0
    \qquad
    \ar(e) = 1
    \qquad
    \ar(s) = 2 \\
    & \ar(R) =
    \ar(G) =
    \ar(O) =
    \ar(P) =
    \ar(N) = 1\\
    & \ar(S) = 3
  \end{align*}
  Thus, this signature has 3 function, 6 predicate symbols, and $\zero$ as the only constant.
  The partitions of the signature are given by
  \begin{align*}
    & \FSym_{0} = \set{\zero}
    & & \FSym_{1} = \set{e}
    & & \FSym_{2} = \set{s}
    & & \FSym_{3} = \emptyset \\
    & \PSym_{0} = \emptyset
    & & \PSym_{1} = \set{R, G, O, P, N}
    & & \PSym_{2} = \emptyset
    & & \PSym_{3} = \set{S}
  \end{align*}
  and for $k > 3$ by $\FSym_{k} = \PSym_{k} = \emptyset$.
\end{example}

\begin{dialogue}
  \spC The signature from \cref{ex:routing-signature} has only a finite number of symbols.
  Is this missing in \cref{def:fol-signature}?
  \spRag No, you can have arbitrarily large signatures.
  For instance, the set $\setDef{\sym{n}}{n \in \N}$ could be a perfectly valid set of function
  symbols.
  The usefulness of such a large signature is, however, another question.
  It is often better to represent natural numbers in the language of FOL, but for that
  we need to first talk about terms.
\end{dialogue}

\begin{definition}{}{fol-terms}
  Let $\Lang$ be a signature $(\FSym, \PSym, \ar)$ and
  \inlinedefbox{$\Var$} a (countable) set of object variables.
  The set \inlinedefbox{$\Term$} or \inlinedefbox{$\Term(\Lang)$} of \termdefpl{term} or
  \notion{$\Lang$-terms}, is the set closed under the following three rules
  \begin{equation*}
    {\begin{prooftree}[center=false]
        \hypo{x \in \Var}
        \infer1{x \in \Term}
      \end{prooftree}}
    \quad
    {\begin{prooftree}[center=false]
        \hypo{c \in \FSym_{0}}
        \infer1{c \in \Term}
      \end{prooftree}}
    \quad
    {\begin{prooftree}[center=false]
        \hypo{f \in \FSym_{n}}
        \hypo{t_{1} \in \Term}
        \hypo{\dotsm}
        \hypo{t_{n} \in \Term}
        \infer4{f(t_{1}, \dotsc t_{n}) \in \Term}
      \end{prooftree}}
  \end{equation*}
  and that fulfils the following \emph{iteration property}:
  for any set $X$ together with maps $I_{\Var} \from \Var \to X$, $I_{0} \from \FSym_{0} \to X$
  and $I_{f} \from X^{\ar(f)} \to X$ for all $f \in \FSym$, there is a unique map
  $I \from \Term \to X$ with
  \begin{align*}
    I(x) & = I_{\Var}(x) \\
    I(c) & = I_{0}(c) \\
    I(f(t_{1}, \dotsc, t_{n})) & = I_{f}(I(t_{1}), \dotsc, I(t_{n})) \, .
  \end{align*}
\end{definition}

\begin{dialogue}
  \spC Oh my \dots{}
  \spRag Don't worry, I will slowly walk you through this definition.
\end{dialogue}

First off, you could think of terms as given by a context free grammar:
\begin{equation*}
  t \cce x \mid c \mid f(t, \dotsc, t)
\end{equation*}
The problem with this grammar is that it does not take the arity of the symbol $f$ into account.
For instance, if you take the symbol $e$ from \cref{ex:routing-signature}, then this grammar would
allow you to write $e(\zero, e(\zero))$.
This clearly does not make a lot of sense, as $e$ is supposed to be a function symbol with one
argument and not one or two!
In \cref{def:fol-terms}, we used instead the three rules to indicate exactly the arity of the
function symbols.
For instance, we can say $\zero \in \Term$ by the second rule and therefore
$e(\zero) \in \Term$ by the third rule.
However, $e(\zero, e(\zero)) \in \Term$ does not follow from those rules because
$e \not\in \FSym_{2}$.
Therefore, $e(\zero, e(\zero)) \not\in \Term$

Now, this last statement, that the construction is \emph{not} a term, would require some proof.
Such a proof is possible by using the iteration principle, as we will see below.
But the iteration principle gives us even more: it allows us to define maps on terms, just as you
saw for propositional formulas.
For instance, we can define maps that count variables or function symbols in terms, or manipulate
terms.
This will be extremely important once we talk about proof theory for FOL.

For the time being, let us define a map that finds all variables in a term.
Recall that $\Pow(\Var)$ is the powerset of $\Var$, given by
$\Pow(\Var) = \setDef{U}{U \subseteq \Var}$.
\begin{lemma}{}{term-vars}
  Let $\Lang$ be a signature.
  There is a map \inlinedefbox{$\var \from \Term(\Lang) \to \Pow(\Var)$} with
  \begin{align*}
    & \var(x) = \set{x}
    & & \var(c) = \emptyset
    & & \var(f(t_{1}, \dotsc, t_{n})) = \bigcup_{k = 1}^{n} \var(t_{k})
  \end{align*}
  that computes the \glsdisp{term variables}{variables} that appear in $\Lang$-terms.
\end{lemma}
\begin{proof}
  Suppose that the function symbols in $\Lang$ are $\FSym$.
  To use the iteration on terms, we define maps
  $\var_{\Var} \from \Var \to \Pow(\Var)$,
  $\var_{0} \from \FSym_{0} \to \Pow(\Var)$
  and $\var_{f} \from \Pow(\Var)^{\ar(f)} \to \Pow(\Var)$
  for all $f \in \FSym$ with $\ar(f) = n$, by
  \begin{align*}
    & \var_{\Var}(x)  = \set{x}
    && \var_{0}(c)  = \emptyset
    && \var_{f}(U_{1}, \dotsc, U_{n})  = \bigcup_{k = 1}^{n} U_{k}
  \end{align*}
  This gives us, by the iteration principle, the map $\var$ with the properties
  that we were looking for.
\end{proof}

\begin{example}{}{routing-terms}
  With the signature $\Lang$ from \cref{ex:routing-signature} and $r, s \in \Var$, we have that
  $s(r, \zero)$ and $s(r, e(s))$ are $\Lang$-terms.
  The map \cref{lem:term-vars} allows us to compute the variables in those terms:
  \begin{equation*}
    \begin{aligned}[t]
      \var(s(r, \zero))
      & = \var(r) \cup \var(\zero) \\
      & = \set{r} \cup \emptyset \\
      & = \set{r}
    \end{aligned}
    \qquad
    \begin{aligned}[t]
      \var(s(r, e(s)))
      & = \var(r) \cup \var(e(s)) \\
      & = \set{r} \cup \var(s) \\
      & = \set{r} \cup \set{s} \\
      & = \set{r, s}
    \end{aligned}
  \end{equation*}

  Note that there are also other terms that may not have any sensible meaning, given the
  intuition we assigned to the symbols.
  For instance, $e(s(r, \zero))$ is a valid term, but what is ``the last index in the position
  at the beginning of a path''?
\end{example}

The way we defined the map $\var$ in \cref{lem:term-vars} is akin to recursive definitions that you
may find in functional programming.
We will usually just define maps on terms through equations that can \emph{in principle} be obtained
by defining the map by appealing to the formal iteration property, but without explicitly using this
principle.
For instance, we can define the height of terms as a map $h \from \Term \to \N$ by three equations:
\begin{align*}
  h(x) & = 0 \\
  h(c) & = 0 \\
  h(f(t_{1}, \dotsc, t_{n})) & = 1 + \max\setDef{h(t_{k})}{1 \leq k \leq n}
\end{align*}
Clearly, $h$ could be obtained by iteration of appropriate maps but these three equations are easier
to read and convey directly the behaviour of $h$.

From the iteration principle, we can also obtain an induction principle.
\begin{theorem}{Term Induction}{term-induction}
  Let $P \subseteq \Term(\Lang)$ be a property of $\Lang$-terms.
  Suppose that $\Var \subseteq P$, $\FSym_{0} \subseteq P$, and for all
  $f \in \FSym$ and  $t_{1}, \dotsc, t_{\ar(f)} \in P$ we have
  $f\parens*{t_{1}, \dotsc, t_{\ar(f)}} \in P$.
  Then $\Term(\Lang) \subseteq P$.
\end{theorem}
\begin{proof}
  We put $I_{\Var}(x) = x$, $I_{0}(c) = c$ and
  $I_{f}(t_{1}, \dotsc, t_{\ar(f)}) = f(t_{1}, \dotsc, t_{\ar(f)})$.
  By the iteration principle, we get a map $I \from \Term(\Lang) \to P$ with
  $I(t) = t$ for all terms $t$, and by uniqueness $I$ must be an inclusion map.
  Therefore, $\Term(\Lang) \subseteq P$.
\end{proof}

Let us come back to \cref{ex:routing-terms}.
We saw at the end that there are terms that we may not care about or that we consider as
``non-sense''.
This shows that terms should really be only thought of as providing a way of representing
objects, or rather shapes of objects, that we may want to reason about.
And for that, we will need to talk about formulas in first-order logic.
There is a slight complication with variables in FOL, but you will have to wait until the next
chapter of your journey to see this issue come up.
For the moment, we make only a first attempt.

\begin{definition}{A first attempt to define FOL formulas}{fol-formulas-approx}
  Let $\Lang$ be a signature with predicate symbols $\PSym$.
  The set $\Forms$ or $\Forms(\Lang)$ of \notion{(first-order) formulas} or
  \notion{$\Lang$-formulas} is closed under the following rules
  \begin{gather*}
    {\begin{prooftree}[center=false]
        \hypo{P \in \PSym_{n}}
        \hypo{t_{1} \in \Term}
        \hypo{\dotsm}
        \hypo{t_{n} \in \Term}
        \infer4{P(t_{1}, \dotsc t_{n}) \in \Forms}
      \end{prooftree}}
    \qquad
    {\begin{prooftree}[center=false]
        \hypo{\phi \in \Forms}
        \infer1{(\phi) \in \Forms}
      \end{prooftree}}
    \\
    {\begin{prooftree}[center=false]
        \hypo{}
        \infer1{\bot \in \Forms}
      \end{prooftree}}
    \qquad
    {\begin{prooftree}[center=false]
        \hypo{\phi \in \Forms}
        \hypo{\psi \in \Forms}
        \hypo{\Box \in \set{\land, \lor, \to}}
        \infer3{\phi \mathbin{\Box} \psi \in \Forms}
      \end{prooftree}}
    \\
    {\begin{prooftree}[center=false]
        \hypo{x \in \Var}
        \hypo{\phi \in \Forms}
        \infer2{\glssymbol{forall} \in \Forms}
      \end{prooftree}}
    \qquad
    {\begin{prooftree}[center=false]
        \hypo{x \in \Var}
        \hypo{\phi \in \Forms}
        \infer2{\glssymbol{exists} \in \Forms}
      \end{prooftree}}
  \end{gather*}
  and fulfils an iteration property, similar to that of terms.
  The binding precedences extend that of propositional logic:
  \begin{itemize}
  \item $\land$ and $\lor$ bind stronger than $\to$, and
  \item $\all{x}$ and $\exist{x}$ extend to the right as far as possible.
  \end{itemize}
\end{definition}

We will not worry too much about the iteration principle, but you will see how it works
in a moment.
The iteration principle implies also again an induction principle for formulas.
However, we will not need this principle at this stage.

Let us see some examples of formulas and applications of the precedences.
\begin{example}{}{fol-formulas}
  All the formulas from
  \cref{eq:no-collision-formula,eq:path-formula-naive,eq:path-formula-better,eq:routing-formula}
  are all formulas over the signature from \cref{ex:routing-signature}.
\end{example}
As for the precedences, the \cref{tab:pl-precedences-ex} from propositional logic still applies, but
we additionally have the rule for quantifiers, which is illustrated in
\cref{tab:quantifier-parentheses}.
\begin{table}
  \centering
  \begin{tabular}{c|c}
    Formula & With parentheses \\ \hline
    $\all{x} R(x) \to O(x)$ & $\all{x} (R(x) \to O(x))$ \\
    $\all{x} \exist{y} R(x) \to O(y)$ & $\all{x} (\exist{y} (R(x) \to O(y)))$ \\
    $\all{x} R(x) \to \exist{y} O(y)$ & $\all{x} (R(x) \to (\exist{y} O(y)))$
  \end{tabular}
  \caption{Leaving out parentheses in formulas with quantifiers}
  \label{tab:quantifier-parentheses}
\end{table}
The rule that quantifiers extend as far as possible to the right implies that we have to
use parentheses if a quantifier should stay under another logical connective.
For instance, the formulas
\begin{equation}
  \label{eq:fol-parentheses-ex}
  \all{x} R(x) \to O(x)
  \qquad \text{and} \qquad
  (\all{x} R(x)) \to O(x)
\end{equation}
are different and also express very different properties.

This becomes clear when we distinguish between \emph{bound} and \emph{free} variables.
\begin{definition}{}{fol-fv-bv}
  Let $\Lang$ be a signature.
  We define maps on $\Lang$-formulas
  \begin{equation*}
    \glsdisp{free variables}{\fv} \from \Forms \to \Pow(\Var)
    \qquad \text{and} \qquad
    \glsdisp{bound variables}{\bv} \from \Forms \to \Pow(\Var)
  \end{equation*}
  by
  \begin{align*}
    \SwapAboveDisplaySkip
    \fv(P(t_{1}, \dotsc, t_{n})) & = \bigcup_{k = 1}^{n} \var(t_{k})
    & \bv(P(t_{1}, \dotsc, t_{n})) & = \emptyset \\
    \fv(\bot) & = \emptyset
    & \bv(\bot) & = \emptyset \\
    \fv(\phi \mathbin{\Box} \psi) & = \fv(\phi) \cup \fv(\psi)
    & \bv(\phi \mathbin{\Box} \psi) & = \bv(\phi) \cup \bv(\psi) \\
    \fv(\all{x} \phi) & = \fv(\phi) \smallsetminus \set{x}
    & \bv(\all{x} \phi) & = \bv(\phi) \cup \set{x} \\
    \fv(\exist{x} \phi) & = \fv(\phi) \smallsetminus \set{x}
    & \bv(\exist{x} \phi) & = \bv(\phi) \cup \set{x}
  \end{align*}
  where $\smallsetminus$ denotes set difference
  and $\Box \in \set{\land, \lor, \to}$.
  We say that a variable $x$ is \emph{free} (resp. \emph{bound}) in $\phi$,
  if $x \in \fv(\phi)$ (resp. $x \in \bv(\phi)$).
\end{definition}

\begin{example}{}{fol-fv-bv}
  Suppose we have a signature $\Lang$ with unary predicate symbols (one argument) $P$ and $Q$, and
  that $\phi = (\all{x} P(x) \land Q(y)) \to P(x) \lor Q(y)$.
  The variable $y$ is clearly nowhere bound, while $x$ is bound \emph{and} free:
  \begin{equation*}
    (\underbrace{\all{x} P(x) \land Q(x)}_{x \text{ bound}}) \to \underbrace{P(x) \lor Q(y)}_{x, y \text{ free}}
  \end{equation*}
  Formally, we have
  \begin{align*}
    \fv(\phi)
    & = \fv(\all{x} P(x) \land Q(x)) \cup \fv(P(x) \lor Q(y)) \\
    & = (\fv(P(x) \land Q(x)) \smallsetminus \set{x}) \cup \fv(P(x)) \cup \fv(Q(y)) \\
    & = (\fv(P(x)) \cup \fv(Q(x)) \smallsetminus \set{x}) \cup \set{x} \cup \set{y} \\
    & = (\set{x} \smallsetminus \set{x}) \cup \set{x, y} \\
    & = \emptyset \cup \set{x, y} \\
    & = \set{x, y}
  \end{align*}
  and
  \begin{align*}
    \bv(\phi)
    & = \bv(\all{x} P(x) \land Q(x)) \cup \bv(P(x) \lor Q(y)) \\
    & = (\bv(P(x) \land Q(x)) \cup \set{x}) \cup \bv(P(x)) \cup \bv(Q(y)) \\
    & = (\bv(P(x)) \cup \bv(Q(x)) \cup \set{x}) \cup \emptyset \cup \emptyset \\
    & = (\emptyset \cup \set{x}) \cup \emptyset \\
    & = \set{x} \, .
  \end{align*}
\end{example}

\Cref{ex:fol-fv-bv} shows how bound and free variables for a formula can be computed, and that
variables can occur both bound and free.
This is a bit misleading, however, because \emph{at every individual occurrence} a variable is
either bound or free, but never both.
If we apply \cref{def:fol-fv-bv} to the formulas in \cref{eq:fol-parentheses-ex},
then we find that the formula on the left has no free variables and $x$ as bound variable,
while the formula on the right has $x$ both as free and bound variable.
This should give a clear indication that the two formulas are different.

\begin{dialogue}
  \spC But can the formulas still not mean the same thing?
  \spRag Keep in mind that we are talking about syntax for the time being, which has no
  intrinsic meaning!
  That being said, you can read the formula $\all{x} R(x) \to O(x)$ as
  ``$R(x)$ implies $O(x)$ for all $x$''.
  Compare this to the reading of the formula $(\all{x} R(x)) \to O(x)$ as
  ``if $R(y)$ holds for all $y$, then $O(x)$ holds for whatever $x$ is''.
  \spC But now you changed bound $x$ in the second formula to a $y$.
  Of course they read differently then!
  \spRag This is indeed a delicate part of first-order logic that we will not be able to
  fully resolve at this stage.
  But think of it this way: the formula $\all{x} R(x)$ and $\all{y} R(y)$ should express the same
  thing, namely that $R$ holds for all objects.
  Therefore, I renamed $x$ to $y$.
  This resolves the naming conflict and we can clearly distinguish the two formulas.
  But this also shows that \cref{def:fol-formulas-approx} is missing an important aspect of variable
  binding.
  The night is setting in. I'm afraid that we have to end our discussion for today and you will
  have to wait for the resolution of this problem.
  \spC Thank very much! This is a lot to take in.
  \spRag Indeed, but most of what we have discussed is quite straightforward:
  just remember that first-order predicate logic allows us to reason about objects,
  where objects are described syntactically as terms, and we use predicate symbols to describe
  properties of objects.
  Finally, quantifiers are used to describe universal and existential properties.
  You can find an overview over the connectives in \cref{tab:fol-connectives}.
  \spI I think that I understand now roughly how the routing problem can be formally
  described, but I hope that we will learn more about how it can actually be solved!
  \spRag For that, my friend, you will have to continue your journey.
  I will now go home and take a bit of rest.
%  \spI Before you leave us, could you give us one final example?
%  \spRag Of course! In fact, I forgot to tell you how we can reason about numbers in FOL.
\end{dialogue}

\direct{
  Clara and Isaac leave the temple through one of the side arcs.
  Some children have played with their drawing under the tree.
  The robot has gotten a heart and left the board, leaving all constraints behind but those
  scribbled on the side of the board.
  As the wind has started to blow gently, these became unreadable but for a fragment reading:
  ``\dots laws of robotics''.
  Birds came out to use the cooler night to find food and comrades.
  They are making loud chirping noises, while Clara and Isaac leave the temple area for the next
  chapter of their journey.
}

% \begin{example}{}{fol-natural-numbers}
%   Let $\Lang$ be the signature $(\FSym, \PSym, \ar)$ with
%   \begin{align*}
%     & \FSym = \set{\zero, s}
%     \qquad \PSym = \set{E, L} \\
%     & \ar(\zero) = 0
%     \qquad \ar(s) = 1
%     \qquad \ar(E) = \ar(L) = 2
%   \end{align*}
%   Intuitively, the constant $\zero$ represents the number zero and $s$ the successor map
%   on natural numbers, which increases any n
% \end{example}

\begin{table}[ht]
  \textbf{Basic connectives}
  \begin{center}
    \begin{tabular}{c|l|l}
      Connective & Name & Pronunciation \\ \hline
      $\bot$  & Absurdity/Falsity & \\
      % $p, q, \dotsc$ & Propositional variable \\
      $\land$ & Conjunction & $\phi$ and $\psi$ \\
      $\lor$  & Disjunction & $\phi$ or $\psi$ \\
      $\to$   & Implication & $\phi$ implies $\psi$ \\
      $\all{x}$ & Universal quantifier & for all $x$, $\phi$ holds \\
      $\exist{x}$ & Existential quantifier & for some $x$, $\phi$ holds \\
    \end{tabular}
  \end{center}

  \textbf{Derived connectives}
  \begin{center}
    \begin{tabular}{l|p{3.5cm}|l}
      Connective & Name & Definition \\ \hline
      $\neg$  & Negation & $\neg \phi = \phi \to \bot$ \\
      $\top$  & Truth & $\top = \neg \bot$ \\
      $\bimpl$ & Bi-implication%/Logical equivalence
      & $\phi \bimpl \psi = (\phi \to \psi) \land (\psi \to \phi)$ \\
      $\exist1{x}$ & Uniqueness~quantifier (introduced in \cref{chap:fol-limits})
      & $\exist1{x} \phi = \exist{x} \phi \land \unique{x}{x}{\phi}$
      % $\exist1{x}$ & Counting quantifier & for at least $k$ distinct $x$, $\phi$ holds
    \end{tabular}
  \end{center}
  \caption[FOL connectives]{Logical connectives of first-order logic}
  \label{tab:fol-connectives}
\end{table}



\dobib{}

\end{document}

% Local Variables:
% ispell-local-dictionary: "british"
% mode: latex
% TeX-engine: xetex
% End: