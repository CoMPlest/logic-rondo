# Logic Rondo

## Name
Logic Rondo: An Introduction to Logic for Humans and Computers

## Description

These are the lecture notes for the course Introduction to Logic (4031ILOGI)
in the computer science programme of the Leiden University.

## Compiled Document

The current compiled PDF can be found on the [releases page](https://gitlab.com/hbasold/logic-rondo/-/releases/2.1.0) of the repository.

## Usage and Compilation

Development happens currently in the *v2* branch, as the lecture notes undergo a major rewrite.

The main document has to be compiled with XeLaTex and Biber.
To get the index displayed, the easiest is to run *makeglossaries logic-rondo*, which will then execute Xindy with the correct options.

To display the correct git information, the hooks in *git-hooks* have to be copied into the
directory *.git/hooks*:

    > cp git-hooks/* .git/hooks/

## Contributing

If you find any mistakes or have any suggestions, please open an issue here on gitlab.

## Authors and Acknowledgement

I, Henning Basold, would like to thank the students, who participated in the course
Introduction to Logic, for their feedback.

## License

The lecture notes are licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

## Project status

The lecture notes are currently being written.
