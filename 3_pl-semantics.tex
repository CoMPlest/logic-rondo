\documentclass[logic-rondo]{subfiles}
\begin{document}

\beginchapter{3}{Semantics of Propositional Logic}{pl-semantics}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{dialogue}
  \speak{Peirce} Who on Earth are you two fellows bursting into my house and disturbing my studies!
  Is this tin can another concoction by Newcomb or the Southerners to disturb me?
  \spC{} Our apologies for coming in uninvited, Professor Pierce.
  My friend Isaac and I set out to learn logic, and we were told that you may help us with that.
  \speak{Peirce} That is certainly possible. How may I help you?
  \spC{} My friend Isaac here is a robot and his functioning is founded in pure logic, but we
  don't understand it.
  \speak{Peirce} Are you saying that this creation adheres to reason?
  \speak{Isaac} Even better, all my decisions are based on what I can deduce from the facts that
  I know.
  \speak{Peirce} \dots{}, which makes you inherently limited.
  \speak{Isaac} Does it? I would like to understand that better.
  \speak{Peirce} Good, you are following the first rule of logic: the desire to learn.%
  \footnote{``Upon this first, and in one sense this sole, rule of
    reason, that in order to learn you must desire to learn, and in
    so desiring not be satisfied with what you already incline to
    think, there follows one corollary which itself deserves to be
    inscribed upon every wall of the city of philosophy:
    Do not block the way of inquiry.'',
    from C.S. Peirce, \emph{F.R.L. First Rule of Logic}, 1899.}
  Let us see, where do we start?
  \spC{} We paid a visit to Aristotle and learned about the syntax of propositional logic.
  \speak{Peirce} How did you \dots{}? I guess this whole encounter is illogical in any case.
  Well, if you know about syntax, then you know already how to avoid the fallacies of natural
  language.
  But do you know what formulas mean and how they relate?
  \spC{} Kind of, I think we know intuitively what they mean.
  But how they relate is unclear, at least to me.
  \speak{Peirce} In that case, let us talk about semantics and understand the
  signs of logical formulas.
  Once we have that, we can talk about the relation of formulas and how Isaac may attempt logical
  deduction.
\end{dialogue}

\section{Truth Values}
\label{sec:truth-values}

For every propositional formula, we can try to understand under what conditions it represents
a correct or incorrect, a \emph{true} or \emph{false}, proposition.
This is simplest way of assigning \emph{truth values} to formulas.

\begin{table}[ht]
  \centering
  \begin{tabular}{cc|c}
    $p$ & $q$ & $p \land q$ \\\hline
    \FF & \FF & \FF \\
    \FF & \TT & \FF \\
    \TT & \FF & \FF \\
    \TT & \TT & \TT
  \end{tabular}
  \caption{Truth table of conjunction}
  \label{tab:truth-table-conj}
\end{table}

\begin{example}[breakable]{}{truth-table-conj}
  Let $\phi$ be the formula $p \land q$ for some propositional variables $p$ and $q$.
  What would the truth value of $\phi$?
  Is $\phi$ true or false?
  That depends entirely on the truth values of $p$ and $q$, since propositional variables have no
  intrinsic meaning.
  Let us, for simplicity, write $\FF$ instead of false and $\TT$ instead of true.
  We could choose another notation, but this particular notation is short and will turn out
  to be beneficial, not the least because it corresponds to bits and voltage levels in your
  circuits, Isaac!

  Now think about the case that $p$ and $q$ are both true, thus we assume they have both
  $\TT$ as truth value.
  What would be the truth value of the conjunction $p \land q$?
  Clearly, as $p$ \emph{and} $q$ are true, so should be $p \land q$ (read $\land$ out as ``and'').
  Thus, if $p$ and $q$ have the truth value $\TT$, the $\phi$ should have as well the truth value
  $\TT$.

  How about that case that one of them, say $q$, is false and has the truth value $\FF$?
  Then we end up with the question whether ``true and false'' should be true or false.
  As it is not possible that something is true and false at the same time, we deduce that
  $p \land q$ is false in this case and the formula $\phi$ has the truth value $\FF$.

  We can continue like this and prepare the small \cref{tab:truth-table-conj}.
  This table lists all the possible values that the variables $p$ and $q$ can take, together
  with the truth value of $p \land q$ that results from these values.
  Thus, the first two columns list all possibilities, while the last column is
  \emph{computed}~\cite{Anellis12:PeirceTruthfunctionalAnalysis}
  or \emph{deduced} from the first column.
\end{example}

\

\begin{quiz}
  Suppose we prepare a truth table for the formula $p \land q \land r$ with three different
  propositional variables.
  How many rows would the table have?
\end{quiz}
\begin{answer}
  For each of the three variables, there are two possibilities $\FF$ and $\TT$.
  Thus, there are $2^{3} = 8$ different combinations of truth values and each of them gets
  its own row.
\end{answer}

Surely, we could devise truth tables for all kinds of formulas by hand, but what are
truth tables in general and how do they relate to the meaning of formulas?
To understand this, let us consider each row of the truth table in \cref{tab:truth-table-conj}
separately.
Every row assigns truth values to the variables that appear in the formula and then determines
the truth values of the overall formula.
Note that it does not matter what other variables, which do not appear in the formula, have
as truth value.
The following definition can be thought of formalising the values of variables in one row of a
truth table.

\begin{definition}{}{pl-valuations}
  We define the set of \emph{truth values} $\B$ by $\B = \set{\FF, \TT}$.
  A \emph{valuation} is a map $v \from \PVar \to \B$, that is, an assignment of unique truth values
  to all propositional variables.
\end{definition}

You will have noticed that \cref{def:pl-valuations} leads to infinite truth tables.
This is of course rather inconvenient on paper and we will rectify this later.
However, in theoretical investigations this definition is easy to work with.

\section{Boolean Semantics of Propositional Logic}
\label{sec:boolean-semantics-pl}

``Was the reason to introduce truth tables not to determine the truth value of formulas?'',
Isaac wonders.
``But \cref{def:pl-valuations} only speaks about the values assigned to variables, doesn't it?''.
Absolutely, and the next step is to calculate from a valuation the truth value of a formula.
For this, we will use that $\FF$ and $\TT$ are ordered as numbers, that is, we have
$\FF \leq \TT$, $\FF \leq \FF$ and $\TT \leq \TT$.
Using this information, we can reduce the calculation of truth values to familiar operations
on numbers.
\begin{definition}{}{pl-semantics}
  We define the \termdef{semantic implication} as binary operation
  $\semImpl \from \B \times \B \to \B$ on truth values $x, y \in \B$ by the following case
  distinction.
  \begin{equation*}
    x \semImpl y =
    \begin{cases}
      1, & x \leq y \\
      0, & \text{otherwise}
    \end{cases}
  \end{equation*}
  Given a valuation $v$, we define the \termdef{boolean propositional semantics} of formulas
  iteratively as follows.\todo{Notate v as argument}
  \begin{align*}
    \SwapAboveDisplaySkip
    \sem{-}_{v} & \from \PForm \to \B \\
    \sem{p}_{v} & = v(p) \\
    \sem{\bot}_{v} & = 0 \\
    \sem{\phi \land \psi}_{v} & = \min \set{\sem{\phi}_{v}, \sem{\psi}_{v}} \\
    \sem{\phi \lor \psi}_{v} & = \max \set{\sem{\phi}_{v}, \sem{\psi}_{v}} \\
    \sem{\phi \to \psi}_{v} & = \sem{\phi}_{v} \semImpl \sem{\psi}_{v}
  \end{align*}
\end{definition}



\begin{table}[ht]
  \centering
  \begin{tabular}{cc|c}
    $x$ & $y$ & $x \semImpl y$ \\ \hline
    \FF & \FF & \TT \\
    \FF & \TT & \TT \\
    \TT & \FF & \FF \\
    \TT & \TT & \TT
  \end{tabular}
  \caption{Values of Semantic Implication}
  \label{tab:values-semantic-implication}
\end{table}
\begin{dialogue}
  \spC{} Wait, wait, Professor \dots{}
  Why do we need the semantic implication and what are these strange double brackets?

  \speak{Peirce} The semantic implication is not strictly necessary, but it clarifies the intention
  in the semantics of the \emph{syntactic} implication.
  Also, it makes it easier to understand how implication works, as we can make a table with
  all possible arguments of the operation and the results, see
  \cref{tab:values-semantic-implication}.

  \spC{} Is this not a truth table?

  \speak{Peirce} It indeed looks suspiciously like one and the truth table for implication would
  have exactly the same entries.

  \spC{} So why do we need the semantic implication then?

  \speak{Peirce} Look at the definition, it tells you that the semantic implication is only true
  if the \emph{antecedent}, the first argument $x$, has a truth value that is below that of
  the \emph{consequent} $y$.
  In particular, if $x$ is true, $y$ must be true.
  By just looking at the truth table, you must be particularly confused by the first row, but our
  definition says that implication is just the same as the order of numbers!

  \spC{} Ok, but what about these funny brackets?

  \speak{Peirce} These double brackets, called Scott brackets in honour of Dana Scott, are commonly
  used in denotational semantics.
  They take in a syntactic object, here formulas, and map that to an element of the semantic domain,
  here the truth values in $\B$.
  These brackets have two important features: First, they define a map, which means that
  \emph{every} formula gets assigned a \emph{unique} truth value.
  Second, they are defined by iteration on formulas.
  This means that the truth value of a formula is determined by the truth of its direct subformulas.

  \speak{Isaac} That reminds me of the principle of iteration that Aristotle explained to us.

  \speak{Peirce} Very well observed!
  In fact, the definition of subformulas has proceeded in exactly the same way as the definition
  of the semantics.
\end{dialogue}

But enough chatter!
Let us discuss us some examples.
\begin{example}{}{semantics-conjunction}
  Let $p, q \in \PVar$ and let $v \from \PVar \to \B$ be the valuation defined by
  $v(p) = v(q) = \TT$ and $v(r) = \FF$ for all other $r \in \PVar$.
  Then we have
  \begin{equation*}
    \sem{p \land q}_{v}
    = \min \set{ \sem{p}_{v}, \sem{q}_{v} }
    = \min \set{ v(p), v(q) }
    = \min \set{ \TT, \TT }
    = \TT \, .
  \end{equation*}
\end{example}

You could have read this, of course, already off \cref{tab:truth-table-conj}.
So let us try something more complex.

\begin{example}{}{pl-semantics-complex}
  Suppose $p$ and $q$ are distinct variables and we define a valuation $v$ by
  $v(p) = \FF$, $v(q) = \TT$ and $v(r) = \FF$ for all other $r \in \PVar$.
  Then the semantics of the formula $p \lor (q \to p) \to q \to p$ with respect to $v$ are
  given as follows.
  \begin{align*}
    & \sem{p \lor (q \to p) \to q \to p}_{v} \\
    & = \sem{p \lor (q \to p)}_{v} \semImpl \sem{q \to p}_{v} \\
    & = \sem{p \lor (q \to p)}_{v} \semImpl (\sem{q}_{v} \semImpl \sem{p}_{v}) \\
    & = \sem{p \lor (q \to p)}_{v} \semImpl (v(q) \semImpl v(p)) \\
    & = \max \set{v(p), v(q) \semImpl v(p)} \semImpl (v(q) \semImpl v(p)) \\
    & = \max \set{\FF, \TT \semImpl \FF} \semImpl (\TT \semImpl \FF) \\
    & = \max \set{\FF, \FF} \semImpl \FF \\
    & = \FF \semImpl \FF \\
    & = \TT
  \end{align*}
\end{example}

\todo[inline]{Add result $\sem{\neg \phi}_{v} = 1 - \sem{\phi}_{v}$}

\section{Back to Truth Tables}
\label{sec:truth-tables-again}

Isaac insists: ``Ok, we can calculate now the semantics of formulas for some valuations.
But what about truth tables? These seem to be rather convenient.''
They are indeed!
The following theorem shows that we can reduce the calculation of the semantics to truth tables.
Have you noticed in the \cref{ex:semantics-conjunction,ex:pl-semantics-complex} that the valuations
were defined to be $\FF$ on the variables that did not occur in the formulas?
In fact, we could have given them any value, as they did not appear in the formulas.
We also say that the semantics of formulas is locally determined.
\begin{theorem}{Local Determination}{pl-local-determination}
  Let $\phi$ be a formula.
  If $v_{1}$ and $v_{2}$ are valuations, such that
  $v_{1}(p) = v_{2}(p)$ for all variables $p \in \var(\phi)$ that appear in $\phi$,
  then $\sem{\phi}_{v_{1}} = \sem{\phi}_{v_{2}}$.
\end{theorem}
\begin{proof}
  We proceed by induction on formulas.
  In the base cases $p$ and $\bot$, we have
  \begin{equation*}
    \sem{p}_{v_{1}} = v_{1}(p) = v_{2}(p) = \sem{p}_{v_{2}}
  \end{equation*}
  and
  \begin{equation*}
    \sem{\bot}_{v_{1}} = \FF = \sem{\bot}_{v_{2}} \, .
  \end{equation*}
  For the induction step, we assume that the induction hypothesis (IH) holds for $\phi_{1}$ and $\phi_{2}$,
  that is, $\sem{\phi_{k}}_{v_{1}} = \sem{\phi_{k}}_{v_{2}}$ for $k = 1, 2$.
  We then have
  \begin{enumerate}[label=\roman*)]
  \item for the conjunction that
    \begin{align*}
      \sem{\phi_{1} \land \phi_{2}}_{v_{1}}
      & = \min \set{ \sem{\phi_{1}}_{v_{1}}, \sem{\phi_{2}}_{v_{1}} }
      \tag*{by definition} \\
      & = \min \set{ \sem{\phi_{1}}_{v_{2}}, \sem{\phi_{2}}_{v_{2}} }
      \tag*{by IH} \\
      & = \sem{\phi_{1} \land \phi_{2}}_{v_{2}}
      \tag*{by definition}
    \end{align*}
  \item the analogous argument for disjunction, and
  \item for implication that
    \begin{align*}
      \sem{\phi_{1} \to \phi_{2}}_{v_{1}}
      & = \sem{\phi_{1}}_{v_{1}} \semImpl \sem{\phi_{2}}_{v_{1}}
      \tag*{by definition} \\
      & = \sem{\phi_{1}}_{v_{2}} \semImpl \sem{\phi_{2}}_{v_{2}}
      \tag*{by IH and because $\semImpl$ is a map} \\
      & = \sem{\phi_{1} \to \phi_{2}}_{v_{2}}
      \tag*{by definition}
    \end{align*}
  \end{enumerate}
  Thus, for all formulas $\phi$, the semantics $\sem{\phi}_{v_{1}}$ and $\sem{\phi}_{v_{2}}$ agree,
  meaning that they are determined only by the variables that appear in $\phi$.
\end{proof}

The result of \cref{thm:pl-local-determination} allows us to provide the semantics of any
formula $\phi$ in terms of truth tables.
To this end, we make a table that has one column for every variable that appears in $\phi$ and
one column for the semantics of $\phi$.
The rows of the table will be determined by all the possible values that the variables can attain
and the evaluation of the semantics of $\phi$ under a valuation that is compatible with values of
the variables in a row.
For instance, suppose that only the variables $p$ and $q$ appear in $\phi$.
The truth table would then start like this, where $v$ is any valuation with $v(p) = v(q) = \FF$:
\begin{center}
  \begin{tabular}{cc|c}
    $p$ & $q$ & $\phi$ \\ \hline
    $\FF$ & $\FF$ & $\sem{\phi}_{v}$ \\
    \vdots & \vdots & \vdots
  \end{tabular}
\end{center}

We are now able to construct a truth table for some more complex example.
\begin{example}{}{truth-table-complex}
  Recall that we calculated the semantics of $p \lor (q \to p) \to q \to p$ in
  \cref{ex:pl-semantics-complex} for a specific valuation.
  Using the above recipe, we can now construct the truth table for this formula.
  It can be helpful for complex formula like this one to also add columns for subformulas
  and determine their semantics first.
  Like this, the overall calculation becomes apparent from the table.
  We indicate this by drawing a single vertical line between the columns of the variables
  and the subformulas, which in turn are separated by a double vertical line from the
  formula.
  For the above formula, we then obtain the following truth table.
  \begin{center}
    \begin{tabular}{cc|cc||c}
      $p$ & $q$ & $q \to p$ & $p \lor (q \to p)$ & $p \lor (q \to p) \to q \to p$ \\\hline
      \FF & \FF & \TT & \TT & \TT \\
      \FF & \TT & \FF & \FF & \TT \\
      \TT & \FF & \TT & \TT & \TT \\
      \TT & \TT & \TT & \TT & \TT
    \end{tabular}
  \end{center}
\end{example}

\section{Entailment, Satisfiability, Tautologies}
\label{sec:pl-sat-taut-entailment}

``Great, now we understand what formulas \emph{mean}! But hold on, there seems to be
something funny going on in \cref{ex:truth-table-complex}: all the rows in the table have
the same truth value for the formula'', Isaac inquires.
``Is this correct?''
It is! Such formulas have a special status and we call them \emph{tautologies}.
Before we go there, let me introduce you to another formulation of the semantics for formulas
that is convenient whenever we want to state that a formula is true.

% TODO: Drop v \vDash \phi and only use \Gamma \vDash \phi -> change Thm. 3.9 etc.

\begin{definition}{}{pl-entailment}
  For $\Gamma$ a set of formulas, that is $\Gamma \subseteq \PForm$,
  and a valuation $v$ we define the semantics of $\Gamma$ by
  \begin{equation*}
    \glssymbol{propositional context semantics} = \min\setDef{\sem{\psi}_{v}}{\psi \in \Gamma}
  \end{equation*}
  with $\sem{\Gamma}_{v} = \TT$ if $\Gamma = \emptyset$.
  For a formula $\phi$, we say that $\Gamma$ \termdisp{propositional entailment}{entails} $\phi$,
  written as $\Gamma \sat \phi$,
  if we have for all valuations $v$ that $\sem{\Gamma}_{v} \leq \sem{\phi}_{v}$.
  The set $\Gamma$ contains the \emph{premises} of the entailment.
  In the case that $\Gamma$ is empty, we write $\sat \phi$ instead of $\emptyset \sat \phi$.
  % Let $\phi$ be a formula, $v$ a valuation,
  % and $\Gamma$ a set of formulas, that is, $\Gamma \subseteq \PForm$.
  % We say that
  % \begin{itemize}
  % \item \emph{$v$ satisfies $\phi$}, written $v \sat \phi$,
  %   if $\sem{\phi}_{v} = \TT$
  % \item \emph{$v$ satisfies $\Gamma$}, written $v \sat \Gamma$,
  %   if $v \sat \psi$ for all $\psi \in \Gamma$.
  % \item \emph{$\Gamma$ entails $\phi$}, written as $\Gamma \sat \phi$,
  %   if for all valuations, whenever $v \sat \Gamma$, then also $v \sat \phi$.
  % \end{itemize}
  % If $\Gamma \sat \phi$ does not hold, then we denote this by $\Gamma \nsat \phi$.
\end{definition}

% To prove this theorem, we first need another result that allows us to calculate semantics of
% sets of formulas and to relate entailment to the semantic implication.

% \begin{lemma}{}{pl-entailment-order}
%   Let $\Gamma$ be a set of formulas.
%   We write
%   \begin{equation*}
%     \sem{\Gamma}_{v} = \min\setDef{\sem{\psi}_{v}}{\psi \in \Gamma}
%   \end{equation*}
%   for valuations $v$.
%   Then for all valuations $v$
%   \begin{equation*}
%     v \sat \Gamma \text{ iff } \sem{\Gamma}_{v} = \TT
%   \end{equation*}
%   with $\min \emptyset = \TT$ if $\Gamma = \emptyset$.
%   Consequently, for $\phi \in \PForm$ we have that
%   $\Gamma \sat \phi$ holds iff
%   $\min \parens*{\sem{\Gamma}_{v}} \leq \sem{\phi}_{v}$
%   for all valuations $v$.
% \end{lemma}
% \begin{proof}
%   Left as an exercise.
% \end{proof}

Compare this to the definition of the semantic implication, which holds if and only if the
antecedent is below the consequent.
You will see that, intuitively, $\Gamma \sat \phi$ holds if the conjunctions of all formulas in
$\Gamma$ implies $\phi$.
It might be the case that $\Gamma$ is not a finite set, in which case we cannot form such a
conjunction, but the entailment works even in this case.
Even though an infinite set $\Gamma$ entails a formula $\phi$, only a finite amount of formulas
contributes to the entailment.
Proving this needs, however, tools that are not yet available to us.

What we can do though is to use \cref{def:pl-entailment} to deduce formulas from given premises.
The following \cref{thm:pl-satisfaction} provides some rules that simplify the deduction
process.

\begin{theorem}{}{pl-satisfaction}
  For all formulas $\phi$ and $\psi$ and $\Gamma \subseteq \PForm$ the following holds.
  % \begin{itemize}
  % \item $v \sat p$ iff $v(p) = \TT$
  % \item $v \nsat \bot$
  % \item $v \sat \phi \land \psi$ iff $v \sat \phi$ and $v \sat \psi$
  % \item $v \sat \phi \lor \psi$ iff $v \sat \phi$ or $v \sat \psi$ (or both)
  % \item $v \sat \phi \to \psi$ iff, whenever $v \sat \phi$ holds, then $v \sat \psi$
  % \item $v \sat \phi \to \psi$ iff $v \nsat \phi$ or $v \sat \psi$ (or both)
  % \item $v \sat \neg \phi$ iff $v \nsat \phi$
  % \end{itemize}
  \begin{itemize}
  \item if $\phi \in \Gamma$, then $\Gamma \sat \phi$
  \item if $\Gamma \sat \bot$, then $\Gamma \sat \phi$
  \item $\Gamma \sat \phi \land \psi$ iff $\Gamma \sat \phi$ and $\Gamma \sat \psi$
  \item $\Gamma \sat \phi \lor \psi$ iff $\Gamma \sat \phi$ or $\Gamma \sat \psi$ (or both)
  \item $\Gamma \sat \phi \to \psi$ iff, whenever $\Gamma \sat \phi$ holds, then $\Gamma \sat \psi$
  \item $\Gamma \sat \phi \to \psi$ iff $\Gamma \nsat \phi$ or $\Gamma \sat \psi$ (or both)
  \item $\Gamma \sat \neg \phi$ iff $\Gamma \nsat \phi$
  \end{itemize}
\end{theorem}
\begin{proof}
  We can analyse all the cases separately.
  For instance, suppose $\phi \in \Gamma$.
  By \cref{def:pl-entailment}, we have for any valuations $v$ that
  \begin{equation*}
    \sem{\Gamma}_{v}
    = \min \setDef{\sem{\psi}_{v}}{\psi \in \Gamma}
    \leq \sem{\phi}_{v} \, ,
  \end{equation*}
  which proves our claim.

  We shall also prove the case of conjunction.
  Let $v$ be a valuation.
  Then
  \begin{align*}
    \SwapAboveDisplaySkip
    & \sem{\Gamma}_{v} \leq \sem{\phi \land \psi}_{v} = \min \set{\sem{\phi}_{v}, \sem{\psi}_{v}} \\
    \text{ iff } & \sem{\Gamma}_{v} \leq \sem{\phi}_{v} \text{ and } \sem{\Gamma}_{v} \leq \sem{\psi} \, .
  \end{align*}
  As this holds for any valuation
  $\Gamma \sat \phi \land \psi$
  iff $\Gamma \sat \phi$ $\Gamma \sat \psi$.

  % \begin{align*}
  %   \SwapAboveDisplaySkip
  %   v \sat \phi \land \psi
  %   & \text{ iff } \sem{\phi \land \psi}_{v} = \TT \\
  %   & \text{ iff } \min \set{\sem{\phi}_{v} , \sem{\psi}_{v}} = \TT \\
  %   & \text{ iff } \sem{\phi}_{v} = \TT \text{ and } \sem{\psi}_{v} = \TT \\
  %   & \text{ iff } v \sat \phi \text{ and } v \sat \psi \, .
  % \end{align*}
  All the other cases are proven analogously.
\end{proof}

``Wait, there are two items for the implication?'', Clara objects.
Yes, this is quite special about the Boolean semantics.
The implication can be expressed in this semantics by using negation and disjunction.
But don't be misled, there are also other possible semantics of propositional logic, like Kripke
semantics, that we will unfortunately not touch upon here.
In these semantics, implication cannot be expressed with negation and disjunction.

But we are digressing.
Let us come back to the formula from \cref{ex:truth-table-complex} that was always true.
There are a few classifications of formulas and relations between formulas that allow us to
say how to prove propositions, which means to establish that such propositions are true.
As you may imagine, this is not always possible.
\begin{definition}{}{pl-semantics-proofs}
  Let $\phi, \psi \in \PForm$.
  We say that
  \begin{itemize}
  \item $\phi$ is \emph{satisfiable} if there is a valuation $v$ with $\sem{\phi}_{v} = \TT$.
    Otherwise, we say that $\phi$ is \emph{unsatisfiable};
  \item $\phi$ is a \emph{tautology} if $\sat \phi$, that is, $\emptyset \sat \phi$; and
  \item $\phi$ and $\psi$ are \emph{semantically equivalent}, written $\phi \equiv \psi$,
    if for all valuations $v$, $\sem{\phi}_{v} = \sem{\psi}_{v}$.
  \end{itemize}
\end{definition}

Let me give you some examples to bring this list of terminology to life.
\begin{example}{}{pl-semantics-proof}
  \begin{enumerate}
  \item Let $v(p) = v(q) = \TT$ and $v(r) = \FF$ otherwise.
    Then $\sem{p \land q}_{v} = \TT$ and therefore $p \land q$ is satisfiable.
  \item Let $\phi = p \land \neg p$.
    Then for any valuation $v$, we have
    \begin{equation*}
      \sem{\phi}_{v}
      = \min \set{ v(p), v(p) \semImpl \FF }
      =
      \left.
        \begin{cases}
          \min \set{\FF, \TT}, & v(p) = \FF \\
          \min \set{\TT, \FF}, & v(p) = \TT \\
        \end{cases}
      \right\}
      = \FF
    \end{equation*}
    and thus $\phi$ is unsatisfiable.
  \item The formula $p \to p$ is a tautology:
    Let $v$ be any valuation.
    Then
    \begin{equation*}
      \sem{p \to p}_{v}
      = v(p) \semImpl v(p)
      =
      \left.
        \begin{cases}
          \TT, & v(p) \leq v(p) \\
          \FF, & \text{otherwise}
        \end{cases}
      \right\}
      = \TT
    \end{equation*}
  \item Finally, we have $p \land q \equiv q \land p$:
    For any valuation $v$ we have that
    \begin{equation*}
      \sem{p \land q}_{v}
      = \min \set{v(p), v(q)}
      = \min \set{v(q), v(p)}
      = \sem{q \land p}_{v}
    \end{equation*}
  \end{enumerate}
\end{example}

\section{Semantic Deduction}
\label{sec:pl-semantic-deduction}

Pierce's excursion has left Isaac is confused: ``I can see what tautologies and satisfiable formulas
are, but how does \cref{def:pl-semantics-proofs} relate to deducing propositions?''
First of all, you will appreciate that semantic equivalence says that two formulas have the same
meaning and that one can be replaced by the other, whenever we speak about the semantics of
formulas.
For instance, all tautologies are semantically equivalent.

More importantly, however, we have not talked about the entailment relation
$\Gamma \sat \phi$ between sets of formulas $\Gamma$ and formulas $\phi$, yet.
The idea is that we may want to make some assumptions, like
$r \land u \to w$ and $u \land \neg w$, and deduce a formula like $\neg r$.
``Ooh, that looks familiar! We have seen something like this before in \cref{ex:pl-form-aristotle}
during our discussion with Aristotle.'', Clara's eyes sparkle in delight.
Yes, you can see proofs with assumptions in two ways: either you prove that
\begin{equation*}
  (r \land u \to w)
  \land
  (u \land \neg w)
  \to
  \neg r
\end{equation*}
is a tautology, or you prove the entailment
\begin{equation*}
  \set{r \land u \to w, u \land \neg w} \sat \neg r \, .
\end{equation*}

\begin{theorem}{}{pl-semantic-deduction}
  Let $\Gamma, \Delta \subseteq \PForm$ and $\phi, \psi \in \PForm$.
  Then the following holds.
  \begin{enumerate}[label=\roman*)]
  \item If $\Gamma \subseteq \Delta$ and $\Gamma \sat \phi$, then $\Delta \sat \phi$.
    (\emph{Monotonicity})
  \item \label{semantic-cut}
    If $\Gamma \sat \phi$ and $\Gamma \cup \set{\phi} \sat \psi$, then $\Gamma \sat \psi$.
    (\emph{Transitivity} or \emph{semantic cut})
  \item \label{semantic-deduction}
    $\Gamma \sat \phi \to \psi$ iff $\Gamma \cup \set{\phi} \sat \psi$.
    (\emph{Semantic deduction})
  \item \label{modus-ponens}
    If $\Gamma \sat \phi$ and $\Gamma \sat \phi \to \psi$, then $\Gamma \sat \psi$.
    (\emph{Modus ponens})
  \end{enumerate}
\end{theorem}
\begin{proof}
  Let $\Gamma, \Delta \subseteq \PForm$ and $\phi, \psi \in \PForm$.
  For a valuation $v$, we will write $\Gamma^{v} = \setDef{\sem{\psi}_{v}}{\psi \in \Gamma}$
  to simplify notation.
  \begin{enumerate}[label=\roman*)]
  \item Suppose that $\Gamma \subseteq \Delta$ and $\Gamma \sat \phi$.
    We obtain from that first assumption that $\Gamma^{v} \subseteq \Delta^{v}$ and thus
    $\sem{\Delta}_{v} = \min \Delta^{v} \leq \min \Gamma^{v} = \sem{\Gamma}_{v}$ (note the
    reversed order).
    Combined with the second assumption, we get
    $\sem{\Delta}_{v} \leq \sem{\Gamma}_{v} \leq \sem{\phi}_{v}$ and thus $\Delta \sat \phi$
    as desired.
  \item Suppose that $\Gamma \sat \phi$ and $\Gamma \cup \set{\phi} \sat \psi$, and let $v$ be a
    valuation.
    We get
    \begin{align*}
      & \sem{\Gamma}_{v} \\
      & = \min \set{ \sem{\Gamma}_{v}, \sem{\phi}_{v}}
      \tag*{by the assumption $\sem{\Gamma}_{v} \leq \sem{\phi}_{v}$} \\
      & \leq \sem{\psi}_{v}
      \tag*{by the second assumption}
    \end{align*}
    and thus $\Gamma \sat \psi$.
  \item
    This item requires an intermediate result that is left as an exercise:
    For all $x, y, z \in \B$, we have that
    \begin{equation*}
      x \leq y \semImpl z \text{ iff } \min \set{x, y} \leq z \, .
    \end{equation*}
    Using this result and by applying the \cref{def:pl-entailment} twice, we obtain
    \begin{align*}
      & \sem{\Gamma}_{v} \leq \sem{\phi \to \psi}_{v} = \sem{\phi}_{v} \semImpl \sem{\psi}_{v} \\
      & \text{ iff }
      \sem{\Gamma \cup \set{\phi}}_{v}
      = \min (\Gamma \cup \set{\phi})^{v}
      = \min \set{\sem{\Gamma}_{v}, \sem{\phi}_{v}}
      \leq \sem{\psi}_{v}
    \end{align*}
    and thus $\Gamma \sat \phi \to \psi$ iff $\Gamma \cup \set{\phi} \sat \psi$.
  \item This follows from \cref{semantic-cut,semantic-deduction}.
    \qedhere
  \end{enumerate}
\end{proof}

Clara gets excited: ``We should be able to use \cref{thm:pl-semantic-deduction} to formalise
Aristotle's syllogism, shouldn't we?''.
Yes indeed, this is possible.

\begin{example}{}{pl-semantic-syllogism}
  Let us define $\Gamma = \set{r \land u \to w, u \land \neg w}$ and show that the entailment
  \begin{equation*}
    \Gamma \sat \neg r
  \end{equation*}
  holds.
  By definition of negation and \cref{thm:pl-semantic-deduction}.\ref{semantic-deduction},
  we can instead prove $\Gamma \cup \set{r} \sat \bot$.
  We shall denote $\Gamma \cup \set{r}$ henceforth by $\Gamma_{1}$.
  By applying \cref{thm:pl-satisfaction} repeatedly, we obtain
  \begin{equation*}
    \Gamma_{1} \sat u \text{ and } \Gamma_{1} \sat r
  \end{equation*}
  and therefore
  \begin{equation*}
    \Gamma_{1} \sat u \land r \, .
  \end{equation*}
  Since $r \land u \to w \in \Gamma_{1}$ we can use
  modus ponens~(\cref{thm:pl-semantic-deduction}.\ref{modus-ponens})
  to obtain
  \begin{equation}
    \label{eq:syllogism-deriv-w}
    \Gamma_{1} \sat w \, .
  \end{equation}
  On the other hand, we can apply \cref{thm:pl-satisfaction} to obtain
  \begin{equation}
    \label{eq:syllogism-deriv-not-w}
    \Gamma_{1} \sat \neg w
  \end{equation}
  because $u \land \neg w \in \Gamma_{1}$.
  By applying modus ponens to \cref{eq:syllogism-deriv-w,eq:syllogism-deriv-not-w},
  we find that $\Gamma_{1} \sat \bot$
  and thus $\Gamma \sat \neg r$.
\end{example}

\begin{dialogue}
  \spI This is all good and well, but \cref{ex:pl-semantic-syllogism} has too much
  natural language for my taste!
  How could this kind of reasoning underlie any of my inner workings?
  \spC Indeed, the reasoning requires quite some ingenuity, no offence Isaac, and does
  not lend itself to automatic computation.
  \spI No offence taken!
  \spC Even for me, the reasoning is hard to follow.
  \speak{Peirce} You are absolutely right and I would advise not to carry out deductions
  this way.
  Even though the principles established in \cref{thm:pl-satisfaction,thm:pl-semantic-deduction}
  are useful, they do not tell us how to organise proofs.
  \spC{} Then we should learn how to organise deductions! Can you help us to do that?
  \speak{Peirce} I'm afraid that your time here is up and you I will have to refer you to two other
  logicians.
  The first logician will introduce you to the exciting world of proof theory, which offers
  powerful tools to organise formal deductions and make them computer-verifiable.
  The other logician is more interested in letting computers figure out deductions automatically.
  This is an ideal task to test the limits of your computing machinery, Isaac.

  Then, off you go! I wish you farewell and great continuation of your voyage!
\end{dialogue}

% TODO: Add overview diagram syntax <-> semantics
% TODO: More information: \cite{Stankovic07:HistoricalRemarksSwitchingTheory},
%       \cite{Anellis12:PeirceTruthfunctionalAnalysis}, \cite{Peirce1885:AlgebraOfLogic}


\dobib{}

\doappendix{}

\end{document}

% Local Variables:
% ispell-local-dictionary: "british"
% mode: latex
% TeX-engine: xetex
% End: